'use strict';

const { BigNumber } = require('ethers');
const lodash = require('lodash');
const constants = lodash.mapValues(require('./constants'), BigNumber.from.bind(BigNumber));

const power = (baseN, baseD, expN, expD) => {
  const base = BigNumber.from(baseN).mul(constants.FIXED_1).div(baseD);
  const baseLog = base.lt(constants.OPT_LOG_MAX_VAL) ? optimalLog(base) : generalLog(base);
  const baseLogTimesExp = baseLog.mul(expN).div(expD);
  return baseLogTimesExp.lt(OPT_EXP_MAX_VAL) ? optimalExp(baseLogTimesExp, constants.MAX_PRECISION) : (() => {
    const precision = findPositionInMaxExpArray(baseLogTimesExp);
    return {
      mantissa: generalExp(baseLogTimesExp.shr(constants.MAX_PRECISION.sub(precision)), precision),
      exponent: precision
    };
  })();
};

const generalLog = (x) => {
  const res = BigNumber.from('0');
};
