'use strict';

require('@nomiclabs/hardhat-waffle');
require('hardhat-deploy');
require("hardhat-deploy-ethers");
require('hardhat-gas-reporter');
const ethers = require('ethers');
const gasnow = require('ethers-gasnow');

ethers.providers.BaseProvider.prototype.getGasPrice = gasnow.createGetGasPrice('rapid');

const INFURA_PROJECT_ID = process.env.INFURA_PROJECT_ID || '2f1de898efb74331bf933d3ac469b98d';

const unlockWalletIfSet = () => (process.env.SECRET ? ethers.Wallet.fromEncryptedJsonSync(JSON.stringify(require('./private/deployer')), process.env.SECRET) : ethers.Wallet.createRandom())._signingKey().privateKey;

module.exports = {
  solidity: {
    compilers: [
      {
        version: "0.5.12",
        settings: {
          optimizer: {
            enabled: false,
            runs: 1000
          }
        }
      },
      {
        version: "0.6.12",
        settings: {
          optimizer: {
            enabled: false,
            runs: 1000
          }
        }
      }
    ]
  },
  networks: {
    rinkeby: {
      accounts: [ unlockWalletIfSet() ],
      url: new ethers.providers.InfuraProvider('rinkeby', INFURA_PROJECT_ID).connection.url,
      chainId: 4
    },
    mainnet: {
      accounts: [ unlockWalletIfSet() ],
      url: new ethers.providers.InfuraProvider('mainnet', INFURA_PROJECT_ID).connection.url,
      chainId: 1
    }
  },
  mocha: {
    timeout: 0,
    grep: process.env.GREP
  }
}

