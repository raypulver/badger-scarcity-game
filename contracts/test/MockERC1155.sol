// SPDX-License-Identifier: UNLICENCED
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";

contract MockERC1155 is ERC1155 {
    uint256 public constant NF1 = 1;
    uint256 public constant NF2 = 2;
    uint256 public constant NF3 = 3;

    constructor() public ERC1155("https://test.com") {
        _mint(msg.sender, NF1, 200, "");
        _mint(msg.sender, NF2, 50, "");
        _mint(msg.sender, NF3, 10, "");
    }
}