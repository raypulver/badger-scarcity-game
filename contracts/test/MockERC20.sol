// SPDX-License-Identifier: UNLICENCED
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract MockERC20 is ERC20 {
  constructor(uint256 initialBalance) public ERC20("bDigg", "bDIGG") {
      _setupDecimals(18);
      _mint(msg.sender, initialBalance);
  }
}
