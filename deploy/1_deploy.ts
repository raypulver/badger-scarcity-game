"use strict";

const hre = require("hardhat");
const { deployments } = hre;

const roots = [
  ethers.BigNumber.from("15000"),
  ethers.BigNumber.from("13000"),
  ethers.BigNumber.from("10000"),
];

const BDIGG_ADDRESS = "0x7e7e112a68d8d2e221e11047a72ffc1065c38e1a";
const MEMELTD_ADDRESS = "0xe4605d46Fd0B3f8329d936a8b258D69276cBa264";

module.exports = async () => {
  const [signer] = await ethers.getSigners();
  const { chainId } = await signer.provider.getNetwork();
  const deployerAddr = await signer.getAddress();
  const { deploy } = deployments;
  let powerLib;
  if (chainId === 1) {
    console.log("deploying PowerLib");
    powerLib = await hre.ethers.getContract(
      "PowerLib"
    );
  } else {
    powerLib = await deploy('PowerLib', {
      contractName: 'PowerLib',
      args: [],
      from: deployerAddr,
      libraries:  {}
    });
  }
  console.log("PowerLib deployed!");

  // Test deployments
  if (chainId !== 1) {
    const testProxyRegistryAddress = hre.ethers.utils.getAddress(
      "0x1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a1a"
    );
    const bDiggSupply = ethers.BigNumber.from("100000000000000000000");
    const ids = [1, 2, 3];

    const memeLtd = await deploy("MemeLtd", {
      contractName: "MemeLtd",
      args: [testProxyRegistryAddress],
      libraries: {},
      from: deployerAddr,
    });
    const bDigg = await deploy("MockERC20", {
      contractName: "MockERC20",
      args: [bDiggSupply],
      libraries: {},
      from: deployerAddr,
    });
    await deploy("BadgerScarcityPool", {
      contractName: "BadgerScarcityPool",
      args: [bDigg.address, memeLtd.address, '260', ids, roots],
      from: deployerAddr,
      libraries: {
        PowerLib: powerLib.address,
      },
    });
  } else {
    // mainnet
    console.log("deploying BadgerScarcityPool");
    const ids = [205, 206, 208];
    const roots = [
      ethers.BigNumber.from("13000"),
      ethers.BigNumber.from("11800"),
      ethers.BigNumber.from("10000"),
    ];
    await deploy("BadgerScarcityPool", {
      contractName: "BadgerScarcityPool",
      args: [BDIGG_ADDRESS, MEMELTD_ADDRESS, '260', ids, roots],
      from: deployerAddr,
      libraries: {
        PowerLib: powerLib.address,
      },
    });
    console.log("BadgerScarcityPool deployed!");
  }
};
