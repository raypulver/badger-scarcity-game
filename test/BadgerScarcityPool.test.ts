import { Contract } from 'ethers';
import { expect, use } from 'chai';
import  { solidity } from 'ethereum-waffle';
const chai = require('chai');

const hre = require("hardhat");
const { deployments, ethers } = hre;
const { BigNumber } = ethers;
const isGreaterThanOrEqualTo = BigNumber.prototype.gte;
const isLessThanOrEqualTo = BigNumber.prototype.lte;

use(solidity);

// Helper to assert returned bDigg value with certain tolerance due to Smart Contract's math precision
const withinTolerance = (actual: any, expected: any, delta: any) => isGreaterThanOrEqualTo.bind(actual)(BigNumber.from(expected).sub(Math.floor(Number(expected)*Number(delta)))) && isLessThanOrEqualTo.bind(actual)(BigNumber.from(expected).add(Math.floor(Number(delta)*Number(expected))));
const assertWithinTolerance = (actual: any, expected: any, delta: any) => {
 if (!withinTolerance(actual, expected, delta)) throw Error(`expected ${String(actual)} to be within '${delta}' of ${Number(expected)}`);
}
describe('BadgerScarcityPool contract', () => {

  const roots = [BigNumber.from('15000'), BigNumber.from('13000'), BigNumber.from('10000')];
  const bDiggPool = BigNumber.from('10000000000000000000');
  const gasPrice = BigNumber.from('20000000000');
  const gasLimit = BigNumber.from('9500000');
  const options = { gasPrice: gasPrice, gasLimit: gasLimit };
  const ids = [1, 2, 3];

  let scarcityPool: Contract;
  let powerLib: Contract;
  let bDigg: Contract;
  let memeLtd: Contract;

  beforeEach(async () => {
    await deployments.fixture();

    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    bDigg = await ethers.getContract('MockERC20');
    memeLtd = await ethers.getContract('MemeLtd');
    scarcityPool = await ethers.getContract('BadgerScarcityPool');

    //Whitelists second account to mint NFTs
    await memeLtd.addWhitelistAdmin(MemeLtdOwnerAddr);

    //MemeLtdOwner account creates three types of NFTs with their respective supplies
    await memeLtd.connect(MemeLtdOwner).create(200, 0, 'https://api.test.com/test/', '0x00'); //id = 1, Supply = 200
    await memeLtd.connect(MemeLtdOwner).create(50, 0, 'https://api.test.com/test/', '0x00'); //id = 2, Supply = 50
    await memeLtd.connect(MemeLtdOwner).create(10, 0, 'https://api.test.com/test/', '0x00'); //id = 3, Supply = 10

    await memeLtd.addMinter(MemeLtdOwnerAddr);

    //MemeLtdOwner account mints three types of NFTs
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 1, 1, '0x00'); //id = 1, Minted = 1
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 2, 1, '0x00'); //id = 2, Minted = 1
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 3, 1, '0x00'); //id = 3, Minted = 1
  });

  it('Reverts when deploying with arguments of different length', async () => {
    const testIds = [1, 2];
    powerLib = await ethers.getContract('PowerLib');

    const contractFactory = await ethers.getContractFactory(
      "BadgerScarcityPool",      {
       libraries: {
        PowerLib: powerLib.address,
       }
     }
    );
    await expect(contractFactory.deploy(bDigg.address, memeLtd.address, '260', testIds, roots))
      .to.be.revertedWith('tokenIds must be same size as roots');
  });

  it('bDigg balance of owner matches minted supply', async function () {
    const [ signer ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();

    const ownerBalance = await bDigg.balanceOf(deployerAddr);
    expect(await bDigg.totalSupply()).to.equal(ownerBalance);
  });

  it('ERC1155 balance of owner matches minted supply', async function () {
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    expect(await memeLtd.balanceOf(MemeLtdOwnerAddr, ids[0])).to.equal(1);
    expect(await memeLtd.balanceOf(MemeLtdOwnerAddr, ids[1])).to.equal(1);
    expect(await memeLtd.balanceOf(MemeLtdOwnerAddr, ids[2])).to.equal(1);
  });

  it("Returns used MemeLtd contract's address when requested", async () => {
    await expect(await scarcityPool.memeLtd()).to.equal(memeLtd.address);
  });

  it("Returns used bDigg contract's address when requested", async () => {
    await expect(await scarcityPool.bdigg()).to.equal(bDigg.address);
  });

  it("Returns stored NFT's details for each entry in the tokenPool", async () => {
    expect(await scarcityPool.poolTokens(ids.indexOf(1))).to.eql([BigNumber.from('1'), BigNumber.from('15000')]);
    expect(await scarcityPool.poolTokens(ids.indexOf(2))).to.eql([BigNumber.from('2'), BigNumber.from('13000')]);
    expect(await scarcityPool.poolTokens(ids.indexOf(3))).to.eql([BigNumber.from('3'), BigNumber.from('10000')]);
  });

  it('Sending bDigg to BadgerScarcityPool changes its bDigg reserve', async () => {
    const [ signer ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();

    // Send 10 bDigg tokens to scarcityPool
    await bDigg.approve(deployerAddr, bDiggPool);
    await bDigg.transferFrom(deployerAddr, scarcityPool.address, bDiggPool); //10e18 -> 10 bDigg
    
    // Expect scarcityPool's reserve to contain 10 bDigg
    expect(await scarcityPool.reserve()).to.equal(bDiggPool); //10e18 -> 10 bDigg
  });

  it('Transfers the right amount of bDigg in return of Token ID=1', async () => {
    const amount = 1;
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    // Send 10 bDigg tokens to scarcityPool
    await bDigg.approve(deployerAddr, bDiggPool);
    await bDigg.transferFrom(deployerAddr, scarcityPool.address, bDiggPool); //10e18 -> 10 bDigg
    
    await memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[0], 
      amount, 
      '0x00',
      options
    );
    
    expect(await memeLtd.balanceOf(scarcityPool.address, ids[0])).to.equal(BigNumber.from(amount));
    // Transfers 0.00238528 bDigg to user
    // 10*(1/260)^1.5 = 0.00238528 bDigg
    assertWithinTolerance(await bDigg.balanceOf(MemeLtdOwnerAddr), BigNumber.from('2385283357484778'), 0.05); //0.00238528 bDigg
  });

  it('Transfers the right amount of bDigg in return of Token ID=2', async () => {
    const amount = 1;
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    // Send 10 bDigg tokens to scarcityPool
    await bDigg.approve(deployerAddr, bDiggPool);
    await bDigg.transferFrom(deployerAddr, scarcityPool.address, bDiggPool); //10e18 -> 10 bDigg

    await memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[1], 
      amount, 
      '0x00',
      options
    );
    
    expect(await memeLtd.balanceOf(scarcityPool.address, ids[1])).to.equal(BigNumber.from(amount));
    // Transfers 0.0072532834 bDigg to user
    // 10*(1/260)^1.3 = 0.0072532834 bDigg
    assertWithinTolerance(await bDigg.balanceOf(MemeLtdOwnerAddr), BigNumber.from('7253283418504845'), 0.05);
  });

  it('Transfers the right amount of bDigg in return of Token ID=3', async () => {
    const amount = 1;
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    // Send 10 bDigg tokens to scarcityPool
    await bDigg.approve(deployerAddr, bDiggPool);
    await bDigg.transferFrom(deployerAddr, scarcityPool.address, bDiggPool); //10e18 -> 10 bDigg

    await memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[2], 
      amount, 
      '0x00',
      options
    );

    expect(await memeLtd.balanceOf(scarcityPool.address, ids[2])).to.equal(BigNumber.from(amount));
    // Returns 0.038461538 bDigg to user
    // 10*(1/260)^1 = 0.038461538 bDigg
    assertWithinTolerance(await bDigg.balanceOf(MemeLtdOwnerAddr), BigNumber.from('38461538461538461'), 0.05);
  });

  it('Transfers the right amount of bDigg in return of several differnt Tokens in a row', async () => {
    const amount = 1;
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    // Send 10 bDigg tokens to scarcityPool
    await bDigg.approve(deployerAddr, bDiggPool);
    await bDigg.transferFrom(deployerAddr, scarcityPool.address, bDiggPool); //10e18 -> 10 bDigg

    // Send 1 token of ID=1
    await memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[0], 
      amount, 
      '0x00',
      options
    );

    expect(await memeLtd.balanceOf(scarcityPool.address, ids[0])).to.equal(BigNumber.from(amount));
    // Transfers 0.00238528 bDigg to user
    // 10*(1/260)^1.5 = 0.00238528 bDigg
    assertWithinTolerance(await bDigg.balanceOf(MemeLtdOwnerAddr), BigNumber.from('2385283357484778'), 0.05); //0.00238528 bDigg
  
    // Send 1 token of ID=2
    await memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[1], 
      amount, 
      '0x00',
      options
    );

    expect(await memeLtd.balanceOf(scarcityPool.address, ids[0])).to.equal(BigNumber.from(amount));
    // Transfers 0.00725155 bDigg to user and it is added to its balance
    // (10-0.00238528)*(1/259)^1.3 = 0.00725155 bDigg
    assertWithinTolerance(await bDigg.balanceOf(MemeLtdOwnerAddr), BigNumber.from('9636836662367095'), 0.05); //0.00238528+0.00725155 = 0.00972381 bDigg

    // Send 1 token of ID=3
    await memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[2], 
      amount, 
      '0x00',
      options
    );

    expect(await memeLtd.balanceOf(scarcityPool.address, ids[0])).to.equal(BigNumber.from(amount));
    // Transfers 0.0387223 bDigg to user and it is added to its balance
    // (10-0.009636836)*(1/258)^1 = 0.0387223 bDigg
    assertWithinTolerance(await bDigg.balanceOf(MemeLtdOwnerAddr), BigNumber.from('48359174504761021'), 0.05); //0.00972381+0.0387223 = 0.048359174 bDigg
  
    // User mints one more token of ID=1
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 1, 1, '0x00'); //id = 1, Minted = 2

    // Send 1 token of ID=1
    await memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[0], 
      amount, 
      '0x00',
      options
    );

    expect(await memeLtd.balanceOf(scarcityPool.address, ids[0])).to.equal(BigNumber.from(amount*2));
    // Transfers 0.0024154 bDigg to user and it is added to its balance
    // (10-0.0483591)*(1/257)^1.5 = 0.0024154 bDigg
    assertWithinTolerance(await bDigg.balanceOf(MemeLtdOwnerAddr), BigNumber.from('50774607575291446'), 0.05); //0.0483591+0.0024154 = 0.0507746 bDigg
  });

  it('Reverts when receives a Batch of different tokens', async () => {
    const amounts = [4, 2, 1];
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    // Mints more NFTs of each
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 1, 3, '0x00'); //id = 1, Minted = 4
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 2, 1, '0x00'); //id = 2, Minted = 2
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 3, 1, '0x00'); //id = 3, Minted = 2
    
    // Send 10 bDigg tokens to scarcityPool
    await bDigg.approve(deployerAddr, bDiggPool);
    await bDigg.transferFrom(deployerAddr, scarcityPool.address, bDiggPool); //10e18 -> 10 bDigg
    
    // Sendign batches are disabled: should revert
    await expect(memeLtd.connect(MemeLtdOwner).safeBatchTransferFrom (
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids, 
      amounts, 
      '0x00',
      options
    )).to.be.revertedWith('batch transfers unsupported');
  });

  it('Reverts when more than 1 token is sent', async () => {
    const amount = [5];
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    // Mintrs 4 more NFTs
    await memeLtd.connect(MemeLtdOwner).mint(MemeLtdOwnerAddr, 1, 4, '0x00'); //id = 1, Minted = 5

    await expect(memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[0], 
      amount, 
      '0x00',
      options
    )).to.be.revertedWith('can only transfer one token at a time');
  });

  it('Reverts when less than 1 token is sent', async () => {
    const amount = [0];
    const [ signer, MemeLtdOwner ] = await hre.ethers.getSigners();
    const MemeLtdOwnerAddr = await MemeLtdOwner.getAddress();

    await expect(memeLtd.connect(MemeLtdOwner).safeTransferFrom(
      MemeLtdOwnerAddr, 
      scarcityPool.address, 
      ids[0], 
      amount, 
      '0x00',
      options
    )).to.be.revertedWith('can only transfer one token at a time');
  });

  it('Sending non-memeLtd ERC1155 to BadgerScarcityPool reverts', async () => {
    const [ signer ] = await hre.ethers.getSigners();
    const deployerAddr = await signer.getAddress();
    const amount = 1;
    // Deploy new ERC1155 contract
    const testERC1155 = await (await ethers.getContractFactory("MockERC1155")).deploy();

    // Transfer token from new ERC1155 contract to scarcityPool and expect to revert
    await expect(testERC1155.safeTransferFrom(deployerAddr, scarcityPool.address, 1, amount, '0x00'))
    .to.be.revertedWith('can only send MemeLtd tokens');
  });
  
});
