'use strict';

const hre = require('hardhat');

async function main() {
  const pool = await hre.ethers.getContract('BadgerScarcityPool');
  const tx = await pool.transferOwnership(process.env.ADDRESS || '0xB65cef03b9B89f99517643226d76e286ee999e77');
  console.error(tx.hash);
  const { events } = await tx.wait();
  events.forEach((v) => {
    console.log(v.name + ': ' + v.args.map((v) => hre.ethers.utils.hexlify(v)));
  });
}

main().then(() => {
  process.exit(0);
}).catch((err) => {
  console.error(err);
  process.exit(1);
});
